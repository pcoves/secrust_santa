# Secrust Santa

> A simple way to organize your *Secret Santas*, written in Rust

## Usage

### Configuration

In a `.env` file or using environment variables, set the folowing:
```env
SMTP_LOGIN=login
SMTP_PASSWORD=password
SMTP_SERVER=server
SMTP_PORT=port
```

### Input

Using either `json` or `yaml` format, specify participants

#### Examples

> `config.toml`

```toml
name = "Secrust Santa"
address = "no-reply@host.tld"

[[participants]]
name = "Foo"
email = "foo@host0.tld"

[[participants]]
name = "Bar"
email = "bar@host1.tld"

[[participants]]
name = "Baz"
email = "baz@host2.tld"
```

### Call

```bash
secrust_santa [-d/--dry-run]
```

If the message `All good, Let's get schwifty` appears, inputs are ok and you can remove the `-d` to actually send the emails.
