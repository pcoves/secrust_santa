use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

#[derive(Debug, Deserialize, Serialize)]
pub struct Participants(Vec<Participant>);

impl Deref for Participants {
    type Target = Vec<Participant>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Participants {
    pub fn shuffle(&mut self) {
        self.0.shuffle(&mut rand::thread_rng());
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Participant {
    pub name: String,
    pub email: String,
}
