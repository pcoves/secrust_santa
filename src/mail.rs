use anyhow::{anyhow, Result};
pub use lettre::Message;
use std::ops::Deref;

use crate::participant::Participant;

#[derive(Debug)]
pub struct Mail(Message);

impl Deref for Mail {
    type Target = Message;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Mail {
    pub fn new(
        gifter: &Participant,
        gifted: &Participant,
        mail: &str,
        from_name: &str,
        from_address: &str,
    ) -> Result<Self> {
        let from = format!("{} <{}>", from_name, from_address)
            .parse()
            .map_err(|err| anyhow!("Failed from build FROM email {}", err))?;

        let to = format!("{} <{}>", gifter.name, gifter.email)
            .parse()
            .map_err(|err| anyhow!("Failed to build TO email {}", err))?;

        let body = mail
            .replace("<FROM>", &gifter.name)
            .replace("<TO>", &gifted.name)
            .to_owned();

        let message = Message::builder()
            .from(from)
            .to(to)
            .subject("Ho Ho Ho... Secrust Santa Time!")
            .body(body)
            .map_err(|err| anyhow!("Failed to create email: {err}"))?;

        Ok(Self(message))
    }

    pub fn take(self) -> Message {
        self.0
    }
}
