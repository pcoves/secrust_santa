use clap::Parser;
use dotenv::dotenv;

#[derive(Debug, Parser)]
pub struct Options {
    #[clap(short, long)]
    pub dry_run: bool,

    #[clap(env = "SMTP_LOGIN")]
    pub smtp_login: String,

    #[clap(env = "SMTP_PASSWORD")]
    pub smtp_password: String,

    #[clap(env = "SMTP_HOST")]
    pub smtp_host: String,

    #[clap(env = "SMTP_PORT")]
    pub smtp_port: u16,
}

impl Options {
    pub fn new() -> Self {
        dotenv().ok();
        Self::parse()
    }
}
